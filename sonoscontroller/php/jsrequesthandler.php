<?php

include_once('sharedVars.php');

$data = $_GET;


if(isset($data['cmd'])){
    switch($data['cmd']){
        case 'queue':
			$response = $controller->invoke(ROOMNAME."/queue");
			echo json_encode($response);
            break;
        case 'state':
			$state = $controller->getState(ROOMNAME);
			echo json_encode($state);
		case 'albumart':
			if(isset($data['url'])){
                $url = urldecode($data['url']);

                $ch = curl_init();
                $timeout = 5;
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
                $data = curl_exec($ch);
                curl_close($ch);

                echo base64_encode($data);
            }
    }
}else{
    error('error');
}

function error($text){
    echo $text;
    exit;
}


?>