<?php

include_once('sonoscontroller.php');


define('URL', 'http://192.168.2.84:5005/');
define('JSON_FILE', __DIR__.'/../cmds.json');
define('ROOMNAME', 'jakob');


$controller = new SonosController(URL);
$json = json_decode(file_get_contents(JSON_FILE), true);

function recursiveArrayPrint($array, $intent = null){
    foreach($array as $key => $value){
        if(gettype($value) == "array"){
            echo $intent."<b>$key:</b><br>";
            recursiveArrayPrint($value, $intent.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;');
        }else{
            echo $intent."<b>$key</b>: <i>$value</i><br>";
        }
    }
}

?>