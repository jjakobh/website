<?php

include_once('sharedVars.php');

$data = $_POST;

// $data[cmd] is set
if(isset($data['cmd'])){

	$cmd;
	// get command from 'commands.json'
	foreach ($json as $group) {
		foreach($group['commands'] as $command){
			if($data['cmd'] == $command['name']){
				$cmd = $command;
			}
		}
	}

	// make sure $data[cmd] is valid
	if(isset($cmd)){

		if(isset($cmd['invoke'])){
			handleInvoke($controller, $cmd, $data);
		}else{
			echo '<p><strong>Warning! </strong>command cannot use \'invoke\', TODO</p>';
			exit;
		}

	}
	else{
		echo '<p><strong>Error! </strong>'.$data['cmd'].' is not a valid command</p>';
		exit;
	}
}
else{
	echo '<p><strong>Error! </strong>cmd is not set</p>';
	exit;
}


function handleInvoke($controller, $cmd, $data){
	$invoke = $cmd['invoke'];
	$invoke =  str_replace('{'.'roomname'.'}', ROOMNAME, $invoke);

	foreach($cmd['params'] as $param){
		if(isset($data[$param['name']])){
			$invoke =  str_replace('{'.$param['name'].'}', urlencode($data[$param['name']]), $invoke);
			$invoke = str_replace('+', '%20', $invoke);
		}else{
			echo '<p><strong>Error! </strong>Param '.$param['name'].' was not sent</p>';
			exit;
		}
	}
	$r = $controller->invoke($invoke, true);


	echo "<br>";
	foreach($r as $key => $value){
		echo "<strong>$key</strong>: <i>$value</i><br>";
	}
}


?>