<?php


class SonosController{
    function __construct($url){
        $this->url = $url;
    }

    function invoke($path, $verbose = null){
        $url = $this->url.$path;
        if($verbose) echo $url;
        return $this->readJsonFromURL($url);
    }


    function getState($room){
        return $this->readJsonFromURL($this->url.$room.'/state');
    }
    

    function getQueue($room){
        $url = $this->url.$room.'/state';
        $arr = $this->readJsonFromURL($url);
        /*for($i = 0; $i < count($arr); $i++){
            $arr[$i]['trackNo'] = $i;
        }*/

        return $arr;
    }

    private function readJsonFromURL($url){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);

        $response = json_decode($output, true);
        return $response;
    }
}

?>