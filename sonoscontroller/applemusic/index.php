<?php

include_once('../php/sharedVars.php');

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>SonosController</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- bootstrap css -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<!-- jquery js -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<!-- bootstrap js -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

	<!-- bootstrap-select css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/css/bootstrap-select.min.css">
	<!-- bootstrap-select js -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.1/js/bootstrap-select.min.js"></script>

	<!-- bootstrap-slider css -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/css/bootstrap-slider.min.css">
	<!-- bootstrap-slider js -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-slider/10.0.2/bootstrap-slider.min.js"></script>

	<link rel="stylesheet" href="stylesheet.css">
	<script src="index.js"></script>


</head>
<body>

<nav class="navbar navbar-inverse navbar-static-top">
	<div class="container-fluid">
			<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="../">SonosController</a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
					<ul class="nav navbar-nav">
							<li class="active"><a href="/">Home</a></li>
					</ul>
			</div>
	</div>
</nav>

<div class="container-fluid">
	
	<div class="row">
		<div class="col-lg-8">
			<form action="javascript:void(0)" onsubmit="handleForm($(this));">
				<div class="form-group">
					<label for="term">Search term:</label>
					<input type="text" class="form-control" name="term">
				</div>
				<div class="form-group">
					<label for="usr">Media type:</label>
					<select id="cmdselect" name="media" class="selectpicker" data-live-search="true" data-width="100%" data-style="btn-primary">
						<option>music</option>
						<option>musicVideo</option>
						<option>movie</option>
						<option>podcast</option>
						<option>all</option>
						<option>audiobook</option>
						<option>shortFilm</option>
						<option>tvShow</option>
						<option>software</option>
						<option>ebook</option>
					</select>
				</div>
				<div class="form-group">
					<label for="limit">Limit:</label>
					<input type="number" class="form-control" value="10" name="limit">
				</div>
				<button id="submit" type="submit" class="btn btn-primary">Search</button>
			</form>
		</div>
		<div id="results" class="col-lg-4">
			<ul id="resultslist">
			</ul>
		</div>

	</div>

</div>

</body>
</html>
