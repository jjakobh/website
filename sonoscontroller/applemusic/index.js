function handleForm(form){
    var serialized = form.serialize().replace('%20', '+');

    console.log(serialized);


    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange=function() {
        if (xhr.readyState == 4) {
            var list = $("#resultslist");
            list.empty();

            var result = JSON.parse(this.responseText);
            $(result['results']).each(addToResultDiv);
        }
    }
    xhr.open("POST", "https://itunes.apple.com/search", true);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(serialized);
    console.log("start");
}

function addToResultDiv(index, song){
    console.log(song);

    var list = $("#resultslist");

    var li = `
    <li>
        <div class="row">
            <div class="col-lg-4">
                <img alt="" class="img-responsive queueimg" src="IMAGE">
            </div>
            <div class="col-sm-8">
                <h4>HEADER <small>HEADERSMALL</small></h4>
                <h5>DESCRIPTION</h5>
                <p><em>PITALIC</em></p>
            </div>
        </div>
    </li>
    `;

    switch(song['kind']){
        case "music":
            li = li.replace("IMAGE", song['artworkUrl100'])
                .replace("HEADER", song['trackName'])
                .replace("HEADERSMALL", song['artistName'])
                .replace("DESCRIPTION", song['collectionName'])
                .replace("PITALIC", song['trackId'])
            break;
        case "music-video":
            li = li.replace("IMAGE", song['artworkUrl100'])
                .replace("HEADER", song['trackName'])
                .replace("HEADERSMALL", song['artistName'])
                .replace("DESCRIPTION", song['primaryGenreName'])
                .replace("PITALIC", song['trackId'])
            break;
        case "feature-movie":
            li = li.replace("IMAGE", song['artworkUrl100'])
                .replace("HEADER", song['trackName'])
                .replace("HEADERSMALL", song['artistName'])
                .replace("DESCRIPTION", song['primaryGenreName'])
                .replace("PITALIC", song['trackId'])
            break;
        case "podcast":
            li = li.replace("IMAGE", song['artworkUrl100'])
                .replace("HEADER", song['trackName'])
                .replace("HEADERSMALL", song['artistName'])
                .replace("DESCRIPTION", song['primaryGenreName'])
                .replace("PITALIC", song['trackId'])
            break;
        case "audiobook":
            break;
        case "shortfilm":
            break;
        case "tvShow":
            break
        case "ebook":
            break;
        default:
            li = li.replace("IMAGE", song['artworkUrl100'])
                .replace("HEADER", song['trackName'])
                .replace("HEADERSMALL", song['artistName'])
                .replace("DESCRIPTION", song['collectionName'])
                .replace("PITALIC", song['trackId'])
            break;
    }

    


    list.append(li);
    list.append("<hr>");


    
}