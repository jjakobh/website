// shared accross js
var commands;


// generates bootstrap-select for selecting cmd
function generateCmdSelect(){
    showGroupNames = false;
    selectpicker = $("#cmdselect")

    for(var i = 0; i < commands.length; i++){
        var optgroup = $("<optgroup>", {label: commands[i].groupName})
        for(var j = 0; j < commands[i].commands.length; j++){
            var element = $("<option>"+commands[i].commands[j].name+"</option>");
            element.appendTo(showGroupNames?optgroup:selectpicker);
        }
        if(showGroupNames){
            optgroup.appendTo(selectpicker);
        }
        else {
            if(i != commands.length - 1){
                var divider = $("<option>", {"data-divider": true});
                divider.appendTo(selectpicker);
            }
        }
    }

    selectpicker.selectpicker('refresh');
    
}


// generates form with parameters from commands.json
function generateForm(cmdName){
    
    // defaults to 'say'
    cmdName = cmdName?cmdName:"say";

    // find cmd
    var cmd;
    for(var i = 0; i < commands.length; i++){
        for(var j = 0; j < commands[i].commands.length; j++){
            if(cmdName == commands[i].commands[j].name){
                cmd = commands[i].commands[j];
                break;
            }
        }
    }

    if(cmd == undefined){
        alert("cmd is undefined!");
        return;
    }


    // clear form
    form = $("#parameterform");
    form.empty();
    
    // hidden input for submitting e.G. 'cmd=say'
    var inputcmd = $("<input>", {name: "cmd", id: "inputcmd", hidden: true});
    inputcmd.val(cmdName);
    inputcmd.appendTo(form);

    // for each param
    for(var i = 0; i < cmd.params.length; i++){
        param = cmd.params[i];

        switch(param.type){
            case "text":
                var formGroup = $("<div>", {class: "form-group"});
                var label = $("<label>"+param.displayName+"</label>", {for: "input"+i});
                var input = $("<input>", {id: "input"+i, type: "text", name: param.name, class: "form-control", placeholder: param.placeholder});

                if(param.default) input.val(param.default);

                label.appendTo(formGroup);
                input.appendTo(formGroup);
                break;

            case "int":
                var formGroup = $("<div>", {class: "form-group"});
                var label = $("<label>"+param.displayName+"</label>", {for: "input"+i});
                var input = $("<input>",{
                    id: "input"+i,
                    "data-slider-id": "input"+i,
                    type: "text",
                    name: param.name,
                    "data-slider-min": param.min?param.min:0,
                    "data-slider-max": param.max?param.max:0,
                    "data-slider-step": "1",
                    "data-slider-value": param.default?param.default:0
                });


                label.appendTo(formGroup);
                formGroup.append("&nbsp;");
                input.appendTo(formGroup);

                input.slider();

                break;

            case "boolean":
                var formGroup = $("<div>", {class: "form-check"});
                var label = $("<label>", {class: "form-check-label", for: "input"+i}).html(param.displayName);
                var input = $("<input>", {id: "input"+i, type: "checkbox", class: "form-check-input"});
                
                var inputhidden = $("<input>", {name: param.name, id: "inputhidden"+i, hidden: true});
                inputhidden.val(false);
                input.change(function(e){
                    $("#inputhidden"+this.id.charAt(this.id.length-1)).val(e.currentTarget.checked);
                });

                input.appendTo(formGroup);
                inputhidden.appendTo(formGroup);
                formGroup.append("&nbsp;");
                label.appendTo(formGroup);
                break;
            
            case "select":
            
                var formGroup = $("<div>", {class: "form-group"});

                var select = $('<select/>', {
                    "name": param.name,
                    "id": "input"+i,
                    "data-width": "100%",
                    "data-live-search": "true"
                });
                for (var i = 0; i < param.items.length; i++) {
                    select.append('<option data-subtext=' + param.itemsSearchToken[i] + '>' + param.items[i] + '</option>');
                }

                select.appendTo(formGroup).selectpicker('refresh');
                
                if(param.default){
                    select.selectpicker('val', param.default);
                }
                
                break;

            default:
                alert("unproccesed param type: "+param.type);
                break;
        }
        formGroup.appendTo(form);
    }

    form.append("<br>");

    var submit = $("<button>", {id: "submit", type: "submit", class: "btn btn-primary"});
    submit.html("Submit");
    submit.appendTo(form);

    

}

// handle form submit
function handleForm(form){
    var responselist = $("#responselist");

    serialized = $(form).serialize();
    console.log(serialized);

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange=function() {
        if (xhr.readyState == 4) {
            var li = $("<li>");
            li.append(this.responseText);
            
            responselist.prepend("<hr>");
            responselist.prepend(li);

        }
    }
    xhr.open("POST", "php/cmdhandler.php", true);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(serialized);
}

function say(text, language, volume){
    params = "cmd=say&volume="+volume+"&text="+text+"&language="+language;

    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange=function() {
        if (xhr.readyState == 4) {
        }
    }
    xhr.open("POST", "php/cmdhandler.php", true);
    xhr.setRequestHeader("Content-type","application/x-www-form-urlencoded");
    xhr.send(params);
}

function loadQueue(){
    var queueItemSrc = `
    <li onclick="">
        <div class="row">
            <div class="col-xs-4">
                <img alt="" id="queueimg-{3}" class="img-responsive queueimg" src="https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif">
            </div>
            <div class="col-sm-8">
                <h4>{0} <small>{1}</small></h4>
                <h5>{2}</h5>
            </div>
        </div>
    </li>
    <hr>
    `;

    var queueDiv = $("#queue");

    makeXhttpRequest("/sonoscontroller/php/jsrequesthandler.php?cmd=state", function(status, response){
        var state = JSON.parse(response);
        var aaau = state.currentTrack.absoluteAlbumArtUri;
        var host = '192.168.2.49:1400';// aaau.slice(0, nthIndex(aaau, '/', 3));
        var trackno = state.trackNo;
        if(!host){
            alert("Something went wrong (functions.js): host is undefined");
        }

        makeXhttpRequest("/sonoscontroller/php/jsrequesthandler.php?cmd=queue", function(status, response){
            var queue = JSON.parse(response);

            for(var i = trackno-1; i < queue.length; i++){
                var album = queue[i].album;
                var artist = queue[i].artist;
                var title = queue[i].title;
                var aau = host + queue[i].albumArtUri;
                queueDiv.append($(queueItemSrc.format(title, artist, album, i)));

                if(i-(trackno-1) < 5){
                    makeXhttpRequest("/sonoscontroller/php/jsrequesthandler.php?cmd=albumart&url="+ encodeURIComponent(aau), function(status, response, i){
                        var queueItem = $("#queueimg-"+i);
                        var src = 'data:image/png;base64,'+response;
                        queueItem.attr('src', src);
                    }, i);
                }else{
                    var queueItem = $("#queueimg-"+i);
                    queueItem.attr('src', 'https://images-na.ssl-images-amazon.com/images/I/514nhtrYjmL.png');
                    queueItem.attr('data-src', aau);
                    console.log(queueItem)
                    queueItem.click(function(){
                        console.log('lul')
                        $(this).attr('src', 'https://media.giphy.com/media/3oEjI6SIIHBdRxXI40/giphy.gif');
                        makeXhttpRequest("/sonoscontroller/php/jsrequesthandler.php?cmd=albumart&url="+ encodeURIComponent($(this).attr("data-src")), function(status, response, queueelement){
                            var src = 'data:image/png;base64,'+response;
                            queueelement.attr('src', src);
                        },$(this));
                        queueItem.off('click');
                    });
                    
                    
                }
            }
        });
    });
}

